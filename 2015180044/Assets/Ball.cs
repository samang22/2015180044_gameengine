﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    private Vector3 Direction = new Vector3();
    private float velocity = 5.0f;


    void Start()
    {
    }

    void Update()
    {
        Vector3 tempPos = this.transform.position;
        float tempDeltaTime = Time.deltaTime;
        tempPos.x += this.Direction.x * velocity * tempDeltaTime;
        tempPos.y += this.Direction.y * velocity * tempDeltaTime;
        tempPos.z += this.Direction.z * velocity * tempDeltaTime;
        this.transform.position = tempPos;

        // 공 삭제
        if (
            this.transform.position.x > 300.0f
        ||  this.transform.position.x < -300.0f
        ||  this.transform.position.z > 300.0f
        ||  this.transform.position.z < -300.0f
            )
        {
            Destroy(gameObject);
            Debug.Log("Destroy");
        }
    }

    public void SetDirection(Vector3 _Direction)
    {
        this.Direction.x = _Direction.x;
        this.Direction.y = _Direction.y;
        this.Direction.z = _Direction.z;
    }

    public void SetVelocity(float _vel)
    {
        this.velocity = _vel;
    }

    private void OnCollisionEnter(Collision collision)
    {
        collision.gameObject.GetComponent<Zombie>().SetLife((collision.gameObject.GetComponent<Zombie>().GetLife() - 1));
        Destroy(gameObject);
    }

}
