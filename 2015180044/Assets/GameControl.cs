﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControl : MonoBehaviour
{
    private int m_iWall0Life = 20;
    private int m_iWall1Life = 20;
    private int m_iWall2Life = 20;
    private int m_iWall3Life = 20;

    private bool m_bGameOver = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (m_iWall0Life <= 0
            || m_iWall1Life <= 0
            || m_iWall2Life <= 0
            || m_iWall3Life <= 0
            )
        {
            m_bGameOver = true;
            GameObject GO = Instantiate(Resources.Load("GameOver"), new Vector3(0.0f, 0.0f, 0.0f), new Quaternion(0.0f, 0.0f, 0.0f, 0.0f)) as GameObject;
            GameObject C = GameObject.Find("Canvas");
            GO.transform.parent = C.transform;
            GO.transform.position = new Vector3(Screen.width / 2, Screen.height / 2, 0.0f);

        }
    }


    public void SetWall0Life(int _iLife) { m_iWall0Life = _iLife; }
    public void SetWall1Life(int _iLife) { m_iWall1Life = _iLife; }
    public void SetWall2Life(int _iLife) { m_iWall2Life = _iLife; }
    public void SetWall3Life(int _iLife) { m_iWall3Life = _iLife; }

    public int GetWall0Life() { return m_iWall0Life; }
    public int GetWall1Life() { return m_iWall1Life; }
    public int GetWall2Life() { return m_iWall2Life; }
    public int GetWall3Life() { return m_iWall3Life; }
}
