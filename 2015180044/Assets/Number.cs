﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Number : MonoBehaviour
{
    private int m_iNumber = 0;
    public TextMeshProUGUI myText;



    // Start is called before the first frame update
    void Start()
    {
        m_iNumber = 0;
        myText = GetComponent<TextMeshProUGUI>();
        myText.text = m_iNumber.ToString();

        //myText.GetComponent<Text>().text = m_iNumber.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetNum(int _Num)
    {
        m_iNumber = _Num;
        myText.text = m_iNumber.ToString();
    }

    public int GetNum()
    {
        return m_iNumber;
    }
}
