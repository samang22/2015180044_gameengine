﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceControl : MonoBehaviour
{
    private int m_iChickenNum = 10000;
    private int m_iHumanNum = 10000;
    private int m_iIronNum = 10000;

    private float m_fCurElapsedTime = 0.0f;
    private float m_fHarvestTerm = 3.0f;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        GameObject.Find("ChickenNum").GetComponent<Number>().SetNum(m_iChickenNum);
        GameObject.Find("HumanNum").GetComponent<Number>().SetNum(m_iHumanNum);
        GameObject.Find("IronNum").GetComponent<Number>().SetNum(m_iIronNum);

        m_fCurElapsedTime += Time.deltaTime;
        if (m_fCurElapsedTime >= m_fHarvestTerm)
        {
            m_fCurElapsedTime = 0.0f;
            Harvest();
        }

    }

    private void Harvest()
    {
        List<GameObject> tempTowerList = GameObject.Find("TowerGenerator").GetComponent<TowerGenerator>().GetGameObjects();

        int tempChickenNum = 0;
        int tempIronNum = 0;
        int tempHumanNum = 0;

        for (int i = 0; i < tempTowerList.Count; ++i)
        {
            if (tempTowerList[i].name == "Chicken(Clone)")
            {
                tempChickenNum += 100;
            }
            else if (tempTowerList[i].name == "BlackSmith(Clone)")
            {
                tempIronNum += 100;
            }
            else if (tempTowerList[i].name == "House(Clone)")
            {
                tempHumanNum += 100;
            }
        }

        m_iChickenNum += tempChickenNum;
        m_iIronNum += tempIronNum;
        m_iHumanNum += tempHumanNum;

    }



    public bool UseChicken(int _iNum)
    {
        if (m_iChickenNum > _iNum)
        {
            m_iChickenNum -= _iNum;
            return true;
        }
        return false;
    }
    public bool UseHuman(int _iNum)
    {
        if (m_iHumanNum > _iNum)
        {
            m_iHumanNum -= _iNum;
            return true;
        }
        return false;
    }
    public bool UseIron(int _iNum)
    {
        if (m_iIronNum > _iNum)
        {
            m_iIronNum -= _iNum;
            return true;
        }
        return false;
    }

    public void SetChickenNum(int _iNum)
    {
        m_iChickenNum = _iNum;
    }
    public void SetIronNum(int _iNum)
    {
        m_iIronNum = _iNum;
    }
    public void SetHumanNum(int _iNum)
    {
        m_iHumanNum = _iNum;
    }
    public int GetChickenNum()
    {
        return m_iChickenNum;
    }
    public int GetHumanNum()
    {
        return m_iHumanNum;
    }
    public int GetIronNum()
    {
        return m_iIronNum;
    }
}
