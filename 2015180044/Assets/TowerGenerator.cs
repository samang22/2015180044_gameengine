﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerGenerator : MonoBehaviour
{
    private int m_iCurTowerIndex = -1;

    private GameObject m_goCurTowerPlace = null;


    private List<GameObject> m_listCurTowerList;

    // Start is called before the first frame update
    void Start()
    {
        m_listCurTowerList = new List<GameObject>();
    }

    // Update is called once per frame




    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {

            GameObject target = GetClickedObject();
            if (m_goCurTowerPlace != null)
            {
                if (target.transform == m_goCurTowerPlace.transform)
                {
                    return;
                }
            }


            m_goCurTowerPlace = target;
            if (target == null)
            {
                return;
            }

            // 방어 건물
            if (-1 < m_iCurTowerIndex &&
                m_iCurTowerIndex < 4)
            {
                if (target.name == "TowerPlace0"
                || target.name == "TowerPlace1"
                || target.name == "TowerPlace2"
                || target.name == "TowerPlace3"
                || target.name == "TowerPlace0 (1)"
                || target.name == "TowerPlace1 (1)"
                || target.name == "TowerPlace2 (1)"
                || target.name == "TowerPlace3 (1)"
                || target.name == "TowerPlace0 (2)"
                || target.name == "TowerPlace1 (2)"
                || target.name == "TowerPlace2 (2)"
                || target.name == "TowerPlace3 (2)"
                || target.name == "TowerPlace4"
                || target.name == "TowerPlace5"
                || target.name == "TowerPlace6"
                || target.name == "TowerPlace7"
                || target.name == "TowerPlace4 (1)"
                || target.name == "TowerPlace5 (1)"
                || target.name == "TowerPlace6 (1)"
                || target.name == "TowerPlace7 (1)"
                || target.name == "TowerPlace4 (2)"
                || target.name == "TowerPlace5 (2)"
                || target.name == "TowerPlace6 (2)"
                || target.name == "TowerPlace7 (2)"
                || target.name == "TowerPlace8"
                || target.name == "TowerPlace9"
                || target.name == "TowerPlace10"
                || target.name == "TowerPlace11"
                || target.name == "TowerPlace8 (1)"
                || target.name == "TowerPlace9 (1)"
                || target.name == "TowerPlace10 (1)"
                || target.name == "TowerPlace11 (1)"
                || target.name == "TowerPlace8 (2)"
                || target.name == "TowerPlace9 (2)"
                || target.name == "TowerPlace10 (2)"
                || target.name == "TowerPlace11 (2)"
                || target.name == "TowerPlace12"
                || target.name == "TowerPlace13"
                || target.name == "TowerPlace14"
                || target.name == "TowerPlace15"
                || target.name == "TowerPlace12 (1)"
                || target.name == "TowerPlace13 (1)"
                || target.name == "TowerPlace14 (1)"
                || target.name == "TowerPlace15 (1)"
                || target.name == "TowerPlace12 (2)"
                || target.name == "TowerPlace13 (2)"
                || target.name == "TowerPlace14 (2)"
                || target.name == "TowerPlace15 (2)"
                )
                {
                    CreateTower();
                }
                
            }
            // 생산 건물
            else if (3 < m_iCurTowerIndex &&
                m_iCurTowerIndex < 7)
            {
                if (
                   target.name == "BuildingPlace0"
                || target.name == "BuildingPlace1"
                || target.name == "BuildingPlace2"
                || target.name == "BuildingPlace3"
                || target.name == "BuildingPlace4"
                || target.name == "BuildingPlace5"
                || target.name == "BuildingPlace6"
                || target.name == "BuildingPlace7"
                || target.name == "BuildingPlace8"
                || target.name == "BuildingPlace9"
                || target.name == "BuildingPlace10"
                || target.name == "BuildingPlace11"
                || target.name == "BuildingPlace12"
                || target.name == "BuildingPlace13"
                || target.name == "BuildingPlace14"
                || target.name == "BuildingPlace15"
                || target.name == "BuildingPlace16"
                )
                {
                    CreateProductTower();
                }
            }
        }
    }



    public void SelectCannonTower()
    {
        m_iCurTowerIndex = 0;
    }
    public void SelectCastleTower()
    {
        m_iCurTowerIndex = 1;
    }
    public void SelectTankTower()
    {
        m_iCurTowerIndex = 2;
    }
    public void SelectShootTower()
    {
        m_iCurTowerIndex = 3;
    }
    public void SelectHouse()
    {
        m_iCurTowerIndex = 4;
    }
    public void SelectBlackSmith()
    {
        m_iCurTowerIndex = 5;
    }
    public void SelectChicken()
    {
        m_iCurTowerIndex = 6;
    }

    public int GetCurTowerIndex()
    {
        return m_iCurTowerIndex;
    }


    public void CreateTower()
    {
        GameObject gameObject = null;

        Vector3 newPos = m_goCurTowerPlace.transform.position;
        Quaternion newRot = m_goCurTowerPlace.transform.rotation;
        Vector3 newScale = m_goCurTowerPlace.transform.localScale;

        // check before gen
        GameObject ResCon = GameObject.Find("ResourceControl");

        bool bCheck = false;

        switch (m_iCurTowerIndex)
        {
            case 0:
                if (ResCon.GetComponent<ResourceControl>().UseIron(200))
                {
                    bCheck = true;
                }
                break;
            case 1:
                if (ResCon.GetComponent<ResourceControl>().UseIron(200))
                {
                    bCheck = true;
                }
                break;
            case 2:
                if (ResCon.GetComponent<ResourceControl>().UseIron(200))
                {
                    bCheck = true;
                }
                break;

            case 3:
                if (ResCon.GetComponent<ResourceControl>().UseIron(200))
                {
                    bCheck = true;
                }
                break;

            default:
                break;
        }

        if (!bCheck)
        {
            return;
        }

        // gen
        switch (m_iCurTowerIndex)
        {
            case 0:
                //gameObject = (GameObject)Instantiate(Resources.Load("Prefab/tower_defence"));
                gameObject = /*(GameObject)*/Instantiate(Resources.Load("CannonTowerScript")) as GameObject;
                gameObject.GetComponent<TowerScript>().SetShootTerm(1.5f);
                gameObject.GetComponent<TowerScript>().SetLife(5);
                newScale.x = 0.3f;
                newScale.y = 0.3f;
                newScale.z = 0.3f;
                break;
            case 1:
                gameObject = (GameObject)Instantiate(Resources.Load("CastleTowerScript"), newPos, newRot);
                gameObject.GetComponent<TowerScript>().SetShootTerm(2.0f);
                gameObject.GetComponent<TowerScript>().SetLife(7);
                newScale.x = 0.5f;
                newScale.y = 0.5f;
                newScale.z = 0.5f;
                break;
            case 2:
                gameObject = (GameObject)Instantiate(Resources.Load("TankTowerScript"), newPos, newRot);
                gameObject.GetComponent<TowerScript>().SetShootTerm(3.0f);
                gameObject.GetComponent<TowerScript>().SetLife(10);
                newScale.x = 0.7f;
                newScale.y = 0.7f;
                newScale.z = 0.7f;
                break;

            case 3:
                gameObject = (GameObject)Instantiate(Resources.Load("ShootTowerScript"), newPos, newRot);
                gameObject.GetComponent<TowerScript>().SetShootTerm(0.7f);
                gameObject.GetComponent<TowerScript>().SetLife(3);

                newScale.x = 1.0f;
                newScale.y = 1.0f;
                newScale.z = 1.0f;
                break;

            default:
                break;
        }

        
        gameObject.transform.position = new Vector3(newPos.x, newPos.y, newPos.z);
        gameObject.transform.rotation = new Quaternion(newRot.x, newRot.y, newRot.z, newRot.w);
        gameObject.transform.localScale = new Vector3(newScale.x, newScale.y, newScale.z);

        //m_listCurTowerList.Add(gameObject);

    }
    public void CreateProductTower()
    {
        GameObject gameObject = null;

        Vector3 newPos = m_goCurTowerPlace.transform.position;
        Quaternion newRot = m_goCurTowerPlace.transform.rotation;
        Vector3 newScale = m_goCurTowerPlace.transform.localScale;

        // check before gen
        GameObject ResCon = GameObject.Find("ResourceControl");

        bool bCheck = false;

        switch (m_iCurTowerIndex)
        {
            case 4:
                if (ResCon.GetComponent<ResourceControl>().UseChicken(200))
                {
                    bCheck = true;
                }
                break;
            case 5:
                if (ResCon.GetComponent<ResourceControl>().UseHuman(200))
                {
                    bCheck = true;
                }
                break;
            case 6:
                if (ResCon.GetComponent<ResourceControl>().UseHuman(200))
                {
                    bCheck = true;
                }
                break;

            default:
                break;
        }

        if (!bCheck)
        {
            return;
        }


        switch (m_iCurTowerIndex)
        {
            case 4:
                //gameObject = (GameObject)Instantiate(Resources.Load("Prefab/Fantasy_House_6"), newPos, newRot);
                //gameObject = (GameObject)Instantiate(Resources.Load("Fantasy_House_6"), newPos, newRot);
                gameObject = (GameObject)Instantiate(Resources.Load("House"), newPos, newRot);
                newScale.x = 0.5f;
                newScale.y = 0.5f;
                newScale.z = 0.5f;
                break;
            case 5:
                gameObject = (GameObject)Instantiate(Resources.Load("Blacksmith"), newPos, newRot);
                newScale.x = 0.6f;
                newScale.y = 0.6f;
                newScale.z = 0.6f;
                break;
            case 6:
                gameObject = (GameObject)Instantiate(Resources.Load("Chicken"), newPos, newRot);
                newScale.x = 10.0f;
                newScale.y = 10.0f;
                newScale.z = 10.0f;
                break;
            default:
                break;
        }


        gameObject.transform.position = new Vector3(newPos.x, newPos.y, newPos.z);
        gameObject.transform.rotation = new Quaternion(newRot.x, newRot.y, newRot.z, newRot.w);
        gameObject.transform.localScale = new Vector3(newScale.x, newScale.y, newScale.z);


        m_listCurTowerList.Add(gameObject);
    }

    private GameObject GetClickedObject()
    {
        RaycastHit hit;
        GameObject target = null;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); //마우스 포인트 근처 좌표를 만든다. 

        if (true == (Physics.Raycast(ray.origin, ray.direction * 10, out hit)))   //마우스 근처에 오브젝트가 있는지 확인
        {
            target = hit.collider.gameObject;
        }

        return target;
    }

    public List<GameObject> GetGameObjects()
    {
        return m_listCurTowerList;
    }
}

