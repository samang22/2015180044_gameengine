﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerScript : MonoBehaviour
{
    private float m_fShootTerm = 0.0f;
    private float m_fCurElapsedTime = 0.0f;

    private float m_fHitTerm = 0.0f;
    private float m_fCurHitElapsedTime = 0.0f;
    private bool m_bHit = false;

    private int m_iLife = 3;




    // Start is called before the first frame update
    void Start()
    {
        m_fShootTerm = 2.0f;
        m_fHitTerm = 1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        m_fCurElapsedTime += Time.deltaTime;
        if (m_fCurElapsedTime >= m_fShootTerm)
        {
            m_fCurElapsedTime = 0.0f;
            Shoot();
        }

        if (!m_bHit)
        {
            m_fCurHitElapsedTime += Time.deltaTime;
            if (m_fCurHitElapsedTime >= m_fHitTerm)
            {
                m_fCurHitElapsedTime = 0.0f;
                m_bHit = true;
            }
        }

        if (m_iLife <= 0)
        {
            Destroy(gameObject);
        }

    }

    public void SetShootTerm(float _term)
    {
        m_fShootTerm = _term;
    }

    void Shoot()
    {
        GameObject ball = Instantiate(Resources.Load("BallScript")) as GameObject;
        ball.GetComponent<Ball>().SetVelocity(20.0f);
        Quaternion tempQ = this.transform.rotation;
        Vector3 tempDirection = new Vector3(0.0f, 0.0f, 0.0f);
        tempDirection = tempQ.eulerAngles;

        switch (tempDirection.y)
        {
            case 0: // z 1
                tempDirection.x = 0.0f;
                tempDirection.y = 0.0f;
                tempDirection.z = 1.0f;
                break;
            case 90:     // x 1
                tempDirection.x = 1.0f;
                tempDirection.y = 0.0f;
                tempDirection.z = 0.0f;
                break;
            case 180: // z -1
                tempDirection.x = 0.0f;
                tempDirection.y = 0.0f;
                tempDirection.z = -1.0f;
                break;
            case 270:   // x -1
                tempDirection.x = -1.0f;
                tempDirection.y = 0.0f;
                tempDirection.z = 0.0f;
                break;
            default:
                break;
        }
        //tempDirection = tempDirection.normalized;

        ball.transform.position = this.transform.position;
        ball.transform.localScale = new Vector3(5.0f, 5.0f, 5.0f);

        ball.GetComponent<Ball>().SetDirection(tempDirection);

        if (this.name == "ShootTowerScript(Clone)")
        {
            ball.GetComponent<Ball>().SetVelocity(20.0f);
        }
        else if (this.name == "CannonTowerScript(Clone)")
        {
            ball.GetComponent<Ball>().SetVelocity(10.0f);
        }
        else if (this.name == "TankTowerScript(Clone)")
        {
            ball.GetComponent<Ball>().SetVelocity(10.0f);
        }
        else if (this.name == "CastleTowerScript(Clone)")
        {
            ball.GetComponent<Ball>().SetVelocity(10.0f);
        }



    }
    public void SetLife(int _life)
    {
        m_iLife = _life;
    }
    public int GetLife()
    {
        return m_iLife;
    }

    public void SetHit(bool _hit)
    {
        m_bHit = _hit;
    }
    public bool GetHit()
    {
        return m_bHit;
    }
}
