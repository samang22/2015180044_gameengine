﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    private Vector3 Direction = new Vector3();
    private float velocity = 5.0f;
    private int m_iLife = 3;

    private bool m_bStop = false;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!m_bStop)
        {
            Vector3 tempPos = this.transform.position;
            tempPos += Direction * velocity * Time.deltaTime;
            this.transform.position = tempPos;
        }


        if (m_iLife <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void SetDirection(Vector3 _Direction)
    {
        Direction = _Direction;
    }

    public void SetVelocity(float _vel)
    {
        velocity = _vel;
    }

    public void SetLife(int _life)
    {
        m_iLife = _life;
    }
    public int GetLife()
    {
        return m_iLife;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Wall0"
        || collision.gameObject.name == "Wall1"
        || collision.gameObject.name == "Wall2"
        || collision.gameObject.name == "Wall3"
            )
        {
            GameObject GameConObj = GameObject.Find("GameControl");
            if (collision.gameObject.name == "Wall0")
            {
                GameConObj.GetComponent<GameControl>().SetWall0Life(GameConObj.GetComponent<GameControl>().GetWall0Life() - 1);
            }
            else if (collision.gameObject.name == "Wall1")
            {
                GameConObj.GetComponent<GameControl>().SetWall1Life(GameConObj.GetComponent<GameControl>().GetWall1Life() - 1);
            }
            else if (collision.gameObject.name == "Wall2")
            {
                GameConObj.GetComponent<GameControl>().SetWall2Life(GameConObj.GetComponent<GameControl>().GetWall2Life() - 1);
            }
            else if (collision.gameObject.name == "Wall3")
            {
                GameConObj.GetComponent<GameControl>().SetWall3Life(GameConObj.GetComponent<GameControl>().GetWall3Life() - 1);
            }

            m_bStop = true;
        }
        else
        {
            m_bStop = false;
        }


        if (collision.gameObject.name == "CannonTowerScript(Clone)"
            || collision.gameObject.name == "ShootTowerScript(Clone)"
            || collision.gameObject.name == "CastleTowerScript(Clone)"
            || collision.gameObject.name == "TankTowerScript(Clone)"

            )
        {
            if (collision.gameObject.GetComponent<TowerScript>().GetHit())
            {
                collision.gameObject.GetComponent<TowerScript>().SetLife((collision.gameObject.GetComponent<TowerScript>().GetLife() - 1));
                collision.gameObject.GetComponent<TowerScript>().SetHit(false);
            }

            m_bStop = true;
        }
        else
        {
            m_bStop = false;
        }


    }
}
