﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieGenerator : MonoBehaviour
{
    private float m_fZombieGenCool = 4.0f;
    private float m_fCurElapsedTime = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        m_fCurElapsedTime += Time.deltaTime;
        if (m_fCurElapsedTime > m_fZombieGenCool)
        {
            m_fCurElapsedTime = 0.0f;
            GenerateZombie();
        }
    }

    public void GenerateZombie()
    {
        string tempStrZombiePlace = "";
        GameObject tempGameObj = null;
        for (int i = 0; i < 16; i++)
        {
            tempStrZombiePlace = "ZombiePlace";
            tempStrZombiePlace += i.ToString();

            GameObject tempZombiePlace = GameObject.Find(tempStrZombiePlace);
            Vector3 tempPos = tempZombiePlace.transform.position;
            Quaternion tempQRot = tempZombiePlace.transform.rotation;
            Vector3 tempRot = tempQRot.eulerAngles;
            switch (tempRot.y)
            {
                case 0: // z 1
                    tempRot.x = 0.0f;
                    tempRot.y = 0.0f;
                    tempRot.z = 1.0f;
                    break;
                case 90:     // x 1
                    tempRot.x = 1.0f;
                    tempRot.y = 0.0f;
                    tempRot.z = 0.0f;
                    break;
                case 180: // z -1
                    tempRot.x = 0.0f;
                    tempRot.y = 0.0f;
                    tempRot.z = -1.0f;
                    break;
                case 270:   // x -1
                    tempRot.x = -1.0f;
                    tempRot.y = 0.0f;
                    tempRot.z = 0.0f;
                    break;
                default:
                    break;
            }
            tempGameObj = (GameObject)Instantiate(Resources.Load("Zombie"), tempZombiePlace.transform.position, tempQRot);
            tempGameObj.GetComponent<Zombie>().SetDirection(tempRot);
            tempGameObj.GetComponent<Zombie>().SetVelocity(3.0f);





        }

    }
}
